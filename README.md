# Test

El objetivo del siguiente test es resolver el problema en el lenguaje para el que te estás postulando. Tendrás que incluir los diferentes tests unitarios para validar el correcto funcionamiento de la solución implementada. El entregable deberá permitir probar fácilmente los casos de prueba.

## Formato de entrega
### En un directorio incluir:
* código fuente.
* compilado del proyecto.
* archivo de texto explicando como ejecutar los test unitarios y el proyecto.
* cómo la app tomará el input

## Problema
El desafío es escribir una función "siguienteMasGrande" que toma un entero positivo y devuelve el siguiente entero positivo más grande formado por los dígitos del número original. En caso que no exista un número más grande debe devolver -1.

## Ejemplos
* siguienteMasGrande(12)==21
* siguienteMasGrande(513)==531
* siguienteMasGrande(2017)==2071
* siguienteMasGrande(9)==-1
* siguienteMasGrande(111)==-1
* siguienteMasGrande(531)==-1
* siguienteMasGrande(1234)==1243
